from django.shortcuts import render
from django.http import HttpResponse
import random

# Create your views here.
def home(request):
    return render(request,'generator/home.html')

def password(request):
  
    characters = list('abcdefghijklmnopqrstuvwxyz')

    if request.GET.get('upper'):
        characters.extend(list('ABCDEFGHIJKLMNOPQRSTUVWXYZ'))

    if request.GET.get('numbers'):
        characters.extend(list('1234567890'))

    if request.GET.get('special'):
        characters.extend(list('!@#$%^&*'))

    length = int(request.GET.get('length'))
    p = ''
    for x in range(length):
        p+=random.choice(characters)
    
    return render(request,'generator/pass.html',{'password':p})